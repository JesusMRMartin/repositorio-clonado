import { LitElement, html } from ‘lit-element'; 

class FichaPersona extends LitElement {

	static get properties() {		
		return {			
			name: {type: String},			
			yearsInCompany: {type: Number},			
			photo: {type: Object}			
		};
	}			  	

	render() {
		return html`
			<div>
				<label for="fname">Nombre Completo</label>
				<input type="text" id="fname" name="fname"></input>
				<br />						
				<label for="yearsInCompany">Años en la empresa</label>
				<input type="text" name="yearsInCompany"></input>
				<br />			
				<img src="./img/persona.jpg" height="200" width="200" alt="Foto persona">			
			</div>
		`;
	}
}
customElements.define(‘ficha-persona', FichaPersona)